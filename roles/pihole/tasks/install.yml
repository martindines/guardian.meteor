---
- name: "Update APT package cache"
  apt:
    update_cache: yes
    upgrade: safe
  tags:
    apt-update

- name: "Create Pi-hole group"
  group:
    name: "{{ pihole_group }}"
    state: present

- name: "Create Pi-hole user"
  user:
    name: "{{ pihole_user }}"
    group: "{{ pihole_group }}"
    groups: "{{ pihole_group }}"
    comment: "ad filtering DNS proxy"
    system: True
    shell: "/usr/sbin/nologin"

- name: "Create Pi-hole configuration directory"
  file:
    name: "{{ pihole_setupvars_config_directory }}"
    state: directory
    owner: "{{ pihole_user }}"
    group: "{{ pihole_group }}"
    mode: 0755

- name: "Create Pi-hole configuration"
  template:
    src: "setupVars.conf.j2"
    dest: "{{ pihole_setupvars_config_directory }}/setupVars.conf"
    owner: root
    group: root
    mode: 0644

- stat:
    path: "/usr/local/bin/pihole"
  register: pihole_stat_result

- name: "Download Pi-hole installer"
  get_url:
    url: "{{ pihole_installer_url }}"
    dest: "/tmp/install-pihole.sh"
    mode: 0740
  when: pihole_stat_result.stat.islnk is not defined

- name: "Install Pi-hole"
  shell: "/tmp/install-pihole.sh --unattended"
  when: pihole_stat_result.stat.islnk is not defined

- name: "Set Pi-hole web interface password"
  command: "pihole -a -p {{ pihole_web_interface_password }}"
  when: pihole_web_interface_password is defined

- name: "Replace hardcoded instances of pi.hole in landing page"
  replace:
    path: /var/www/html/pihole/index.php
    regexp: 'pi\.hole'
    replace: "{{ pihole_domain }}"
    backup: yes

- name: "Configure Pi-hole web interface to listen on port {{ pihole_web_interface_port }}"
  lineinfile:
    dest: /etc/lighttpd/lighttpd.conf
    regexp: "^server.port                 = 80"
    line: "server.port                 = {{ pihole_web_interface_port }}"
    backrefs: yes
  when: pihole_web_interface_port != "80"
  notify:
    - "Restart lighttpd"
  tags:
    - role-pihole-install-lighttpd

- name: "Populate Pi-hole adlist source"
  template:
    src: "adlists.list.j2"
    dest: "/etc/pihole/adlists.list"
  tags:
    - role-pihole-install-adlist

- name: "Update Pi-hole adlist"
  command: "pihole -g"
  tags:
    - role-pihole-install-adlist

- name: "Install pyOpenSSL dependencies"
  apt:
    package: "{{ item }}"
    state: installed
  with_items:
    - libffi-dev
    - libssl-dev
  tags:
    - role-pihole-install-pyopenssl

- name: "Ensure python OpenSSL dependencies are installed."
  pip:
    name: pyOpenSSL
    state: present
  tags:
    - role-pihole-install-pyopenssl

- name: "Ensure directory exists for local self-signed TLS certs."
  file:
    path: /etc/letsencrypt/live/{{ pihole_domain }}
    state: directory

- name: "Generate an OpenSSL private key."
  openssl_privatekey:
    path: /etc/letsencrypt/live/{{ pihole_domain }}/privkey.pem

- name: "Generate an OpenSSL CSR."
  openssl_csr:
    path: /etc/ssl/private/{{ pihole_domain }}.csr
    privatekey_path: /etc/letsencrypt/live/{{ pihole_domain }}/privkey.pem
    common_name: "{{ pihole_domain }}"

- name: "Generate a Self Signed OpenSSL certificate."
  openssl_certificate:
    path: /etc/letsencrypt/live/{{ pihole_domain }}/fullchain.pem
    privatekey_path: /etc/letsencrypt/live/{{ pihole_domain }}/privkey.pem
    csr_path: /etc/ssl/private/{{ pihole_domain }}.csr
    provider: selfsigned

- name: "Restart nginx to register configuration"
  systemd:
    name: nginx
    state: restarted

- name: "Ensure lighttpd is enabled and started"
  systemd:
    name: lighttpd
    enabled: yes
    state: started

- name: "Configure cron to update Pi-hole every week"
  replace:
    path: "/etc/cron.d/pihole"
    regexp: '^#(.*updatePihole.*)$'
    replace: '\1'
    backup: yes