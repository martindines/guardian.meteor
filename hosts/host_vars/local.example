---
pihole_setupvars_ipv6_address: ""

dhparam_size: 512 # Specified in playbook install.yml
dhparam_file: "/etc/nginx/dhparams.pem"

nginx_remove_default_vhost: true

nginx_vhosts:
  - listen: "443 ssl http2 default_server"
    server_name: "{{ unifi_controller_domain }}"
    state: "present"
    template: "{{ nginx_vhost_template }}"
    filename: "{{ unifi_controller_domain }}.conf"
    extra_parameters: |
      location /wss/ {
              proxy_pass https://localhost:8443;
              proxy_http_version 1.1;
              proxy_buffering off;
              proxy_set_header Upgrade $http_upgrade;
              proxy_set_header Connection "Upgrade";
              proxy_read_timeout 86400;
      }

      location / {
              proxy_pass https://localhost:8443/;
              proxy_set_header Host $host;
              proxy_set_header X-Real-IP $remote_addr;
              proxy_set_header X-Forward-For $proxy_add_x_forwarded_for;
      }

      ssl on;

      ssl_certificate         /etc/letsencrypt/live/{{ unifi_controller_domain }}/fullchain.pem;
      ssl_certificate_key     /etc/letsencrypt/live/{{ unifi_controller_domain }}/privkey.pem;

      ssl_session_cache shared:le_nginx_SSL:1m;
      ssl_session_timeout 1d;
      ssl_session_tickets off;

      ssl_protocols TLSv1.2;
      ssl_prefer_server_ciphers on;
      ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
      ssl_ecdh_curve secp384r1;

      ssl_stapling on;
      ssl_stapling_verify on;

      add_header Strict-Transport-Security "max-age=15768000; includeSubdomains; preload;";
      add_header Content-Security-Policy "default-src 'self'; connect-src 'self' wss://{{ unifi_controller_domain }}; script-src 'self' 'unsafe-inline'; img-src 'self' data:; style-src 'self' 'unsafe-inline'; font-src 'self'; object-src 'none'";
      add_header Referrer-Policy "no-referrer, strict-origin-when-cross-origin";
      add_header X-Frame-Options SAMEORIGIN;
      add_header X-Content-Type-Options nosniff;
      add_header X-XSS-Protection "1; mode=block";

      ssl_dhparam {{ dhparam_file }};
  - listen: "80"
    server_name: "{{ unifi_controller_domain }}"
    state: "present"
    template: "{{ nginx_vhost_template }}"
    filename: "{{ unifi_controller_domain }}-80.conf"
    extra_parameters: |
      location / {
              return 301 https://$host$request_uri;
      }
  - listen: "443 ssl http2"
    server_name: "{{ pihole_domain }}"
    state: "present"
    template: "{{ nginx_vhost_template }}"
    filename: "{{ pihole_domain }}.conf"
    extra_parameters: |
      location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://localhost:{{ pihole_web_interface_port }};
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_next_upstream error timeout http_502 http_503 http_504;
      }
      ssl on;

      ssl_certificate         /etc/letsencrypt/live/{{ pihole_domain }}/fullchain.pem;
      ssl_certificate_key     /etc/letsencrypt/live/{{ pihole_domain }}/privkey.pem;

      ssl_session_cache shared:le_nginx_SSL:1m;
      ssl_session_timeout 1d;
      ssl_session_tickets off;

      ssl_protocols TLSv1.2;
      ssl_prefer_server_ciphers on;
      ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
      ssl_ecdh_curve secp384r1;

      ssl_stapling on;
      ssl_stapling_verify on;

      add_header Strict-Transport-Security "max-age=15768000; includeSubdomains; preload;";
      add_header Content-Security-Policy "default-src 'self'; connect-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; img-src 'self' data:; style-src 'self' 'unsafe-inline'; font-src 'self'; object-src 'none'";
      add_header Referrer-Policy "no-referrer, strict-origin-when-cross-origin";
      add_header X-Frame-Options SAMEORIGIN;
      add_header X-Content-Type-Options nosniff;
      add_header X-XSS-Protection "1; mode=block";

      ssl_dhparam {{ dhparam_file }};
  - listen: "80"
    server_name: "{{ pihole_domain }}"
    state: "present"
    template: "{{ nginx_vhost_template }}"
    filename: "{{ pihole_domain }}-80.conf"
    extra_parameters: |
      location / {
              return 301 https://$host$request_uri;
      }